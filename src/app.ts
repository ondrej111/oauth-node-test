import {
    Pool,
    ClientConfig,
    QueryResult
} from 'pg';
import * as Koa from 'koa';
import * as KoaRouter from 'koa-router';

const bodyParser = require('koa-body');

import * as config from 'config';
import {Context} from 'koa';

const serverConfig = config.get<{ port: number; }>('server');
const dbConfig = config.get<ClientConfig>('db');
const dbPool: Pool = new Pool(dbConfig);
// const dbClient = new Client(dbConfig);

const app = new Koa();
const koaRouter = new KoaRouter();

koaRouter.post('/tokeninfo', async (ctx: Context, next) => {
    // ZISKAJ POST DATA
    const accessToken = ctx.request['body']['access_token'];
    if (!accessToken) {
        throw new CustomError('Access token is not provided.');
    }

    //  LOAD TOKEN
    const tokensRes: QueryResult<Token> = await processQuery<Token>(
            `SELECT token_id, uuid, client_id, expire_in, access_token, refresh_token, scope, rt_expire_in
                    FROM oauth.tokens WHERE access_token = $1;`, [accessToken]);
    if (tokensRes.rows.length !== 1) {
        throw new CustomError('Not token');
    }
    const token: Token = tokensRes.rows[0];

    // LOAD USER
    const userRes: QueryResult<User> = await processQuery<User>(
        'SELECT uuid, login, name, surname FROM oauth.users WHERE uuid = $1;', [token.uuid]);
    const user: User = userRes.rows[0];

    // UPDATE EXPIRE FOR TOKEN
    const expireIn: string = new Date(new Date().getTime() - (new Date().getTimezoneOffset() * 60000) + 10 * 60000).toISOString();
    await processQuery('UPDATE oauth.tokens SET expire_in = $1 WHERE token_id=$2;',
        [expireIn, token.token_id]);

    ctx.body = {
        'user_id': token.uuid,
        'login': user.login,
        'forename': user.name,
        'surname': user.surname,
        'scope': token.scope,
        'expired_in': ((): number => {
            const now = new Date();
            if (now > token.expire_in) {
                return -1;
            }
            return Number((token.expire_in.getTime() - now.getTime()) / 1000);
        })(),
        'client_id': token.client_id
    };

    next();
});

async function processQuery<K>(query: string, params: (string | any)[]): Promise<QueryResult<K>> {
    // await dbClient.connect();
    const dbClient = await dbPool.connect();
    try {
        return await dbClient.query<K>(query, params);
    } catch (e) {
        throw e;
    } finally {
        await dbClient.release();
    }

}

app.use(bodyParser({
    formidable: {uploadDir: './uploads'},
    multipart: true,
    urlencoded: true
}))
    .use(koaRouter.routes())
    .use(koaRouter.allowedMethods())
    .listen(serverConfig.port, () => {
        console.log(`Koa is listening on port ${serverConfig.port}`);
    });

interface Token {
    token_id: number;
    uuid: string;
    client_id: number;
    expire_in: Date;
    access_token: string;
    refresh_token: string;
    scope: string;
    rt_expire_in: Date;
}

interface User {
    uuid: string;
    login: string;
    name: string;
    surname: string;
}

class CustomError extends Error {
}
