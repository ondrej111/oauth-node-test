"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const pg_1 = require("pg");
const Koa = require("koa");
const KoaRouter = require("koa-router");
const bodyParser = require('koa-body');
const config = require("config");
const serverConfig = config.get('server');
const dbConfig = config.get('db');
const dbPool = new pg_1.Pool(dbConfig);
// const dbClient = new Client(dbConfig);
const app = new Koa();
const koaRouter = new KoaRouter();
koaRouter.post('/tokeninfo', async (ctx, next) => {
    // ZISKAJ POST DATA
    const accessToken = ctx.request['body']['access_token'];
    if (!accessToken) {
        throw new CustomError('Access token is not provided.');
    }
    //  LOAD TOKEN
    const tokensRes = await processQuery(`SELECT token_id, uuid, client_id, expire_in, access_token, refresh_token, scope, rt_expire_in
                    FROM auth.tokens WHERE access_token = $1;`, [accessToken]);
    if (tokensRes.rows.length !== 1) {
        throw new CustomError('Not token');
    }
    const token = tokensRes.rows[0];
    // LOAD USER
    const userRes = await processQuery('SELECT uuid, login, name, surname FROM auth.users WHERE uuid = $1;', [token.uuid]);
    const user = userRes.rows[0];
    // UPDATE EXPIRE FOR TOKEN
    const expireIn = new Date(new Date().getTime() - (new Date().getTimezoneOffset() * 60000) + 10 * 60000).toISOString();
    await processQuery('UPDATE auth.tokens SET expire_in = $1 WHERE token_id=$2;', [expireIn, token.token_id]);
    ctx.body = {
        'user_id': token.uuid,
        'login': user.login,
        'forename': user.name,
        'surname': user.surname,
        'scope': token.scope,
        'expired_in': (() => {
            const now = new Date();
            if (now > token.expire_in) {
                return -1;
            }
            return Number((token.expire_in.getTime() - now.getTime()) / 1000);
        })(),
        'client_id': token.client_id
    };
    next();
});
async function processQuery(query, params) {
    // await dbClient.connect();
    const dbClient = await dbPool.connect();
    try {
        return await dbClient.query(query, params);
    }
    catch (e) {
        throw e;
    }
    finally {
        await dbClient.release();
    }
}
app.use(bodyParser({
    formidable: { uploadDir: './uploads' },
    multipart: true,
    urlencoded: true
}))
    .use(koaRouter.routes())
    .use(koaRouter.allowedMethods())
    .listen(serverConfig.port, () => {
    console.log(`Koa is listening on port ${serverConfig.port}`);
});
class CustomError extends Error {
}
//# sourceMappingURL=app.js.map